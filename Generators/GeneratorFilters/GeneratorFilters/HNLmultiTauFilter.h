/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef GENERATORFILTERS_HNLMULTITAUFILTER_H
#define GENERATORFILTERS_HNLMULTITAUFILTER_H

#include "GeneratorModules/GenFilter.h"

/// Specify the decay of V1/V2 in W->HNL + tau, V1->decay, V2->decay with VH production.
///
/// Example: W -> HNL + tau decay but allows selection of tau decays
/// @author Jacob Bundgaard Nielsen
class HNLmultiTauFilter : public GenFilter {
public:

  HNLmultiTauFilter(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode filterInitialize();
  StatusCode filterFinalize();
  StatusCode filterEvent();

private:

  int m_PDGGrandParent1;
  int m_PDGGrandParent2;
  int m_PDGParent1;
  int m_PDGParent2;
  std::vector<int> m_PDGChild1;
  std::vector<int> m_PDGChild2;

  int m_nHNLtotau;
  int m_nGoodHNLtotau;

};

#endif
