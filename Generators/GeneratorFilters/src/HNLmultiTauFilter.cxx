/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "GeneratorFilters/HNLmultitauFilter.h"

HNLmultitauFilter::HNLmultitauFilter(const std::string& name, ISvcLocator* pSvcLocator)
  : GenFilter(name,pSvcLocator)
{
  /// @todo
  static const int ch1[] =  { 11,12,13,14 };
  static const int ch2[] =  { 111,211 };
  static const std::vector<int> chv1(ch1, ch1 + sizeof(ch1) / sizeof(int));
  static const std::vector<int> chv2(ch2, ch2 + sizeof(ch2) / sizeof(int));

  declareProperty("PDGGrandParent1",m_PDGGrandParent1 = 24);
  declareProperty("PDGGrandParent2",m_PDGGrandParent2 = 50);
  declareProperty("PDGParent1",m_PDGParent1 = 15);
  declareProperty("PDGParent2",m_PDGParent2 = 15);
  declareProperty("PDGChild1",m_PDGChild1 = chv1);
  declareProperty("PDGChild2",m_PDGChild2 = chv2);

  m_nHNLtotau = 0;
  m_nGoodHNLtotau = 0;
}


StatusCode HtoVVFilter::filterInitialize() {
  ATH_MSG_INFO("PDGGrandParent1(H) = " << m_PDGGrandParent1);
  ATH_MSG_INFO("PDGParent1(V)      = " << m_PDGParent1);
  ATH_MSG_INFO("PDGGrandParent2(H) = " << m_PDGGrandParent2);
  ATH_MSG_INFO("PDGParent2(V)      = " << m_PDGParent2);
  if (m_PDGChild1.size()==0)
    ATH_MSG_ERROR("PDGChild1[] not set ");
  if (m_PDGChild2.size()==0)
    ATH_MSG_ERROR("PDGChild2[] not set ");
  for (size_t i = 0; i < m_PDGChild1.size(); ++i) ATH_MSG_INFO("PDGChild1["<<i<<"] = " << m_PDGChild1[i]);
  for (size_t i = 0; i < m_PDGChild2.size(); ++i) ATH_MSG_INFO("PDGChild2["<<i<<"] = " << m_PDGChild2[i]);
  // init
  m_nHtoVV = 0;
  m_nGoodHtoVV = 0;
  return StatusCode::SUCCESS;
}


StatusCode HtoVVFilter::filterFinalize() {
  ATH_MSG_INFO("Statistics of H->VV, V->decay ");
  ATH_MSG_INFO("  ALL  H->VV " << m_nHtoVV);
  ATH_MSG_INFO("  Good H->VV " << m_nGoodHtoVV);
  if (m_nHtoVV != 0) ATH_MSG_INFO("  Fraction   " << double(m_nGoodHtoVV)/double(m_nHtoVV));
  return StatusCode::SUCCESS;
}


StatusCode HtoVVFilter::filterEvent() {
  McEventCollection::const_iterator itr;
  bool okPDGChild1 = false;
  bool okPDGChild2 = false;
  int nGoodParent = 0;
  for (itr = events()->begin(); itr!=events()->end(); ++itr) {
    const HepMC::GenEvent* genEvt = (*itr);
    for (HepMC::GenEvent::particle_const_iterator pitr = genEvt->particles_begin(); pitr != genEvt->particles_end(); ++pitr) {
      if (abs((*pitr)->pdg_id()) == m_PDGParent1 && (*pitr)->status() == 3) {
        HepMC::GenVertex::particle_iterator firstMother = (*pitr)->production_vertex()->particles_begin(HepMC::parents);
        HepMC::GenVertex::particle_iterator endMother = (*pitr)->production_vertex()->particles_end(HepMC::parents);
        HepMC::GenVertex::particle_iterator thisMother = firstMother;
        bool isGrandParent1OK = false;
        for (; thisMother != endMother; ++thisMother) {
          ATH_MSG_INFO(" Parent " << (*pitr)->pdg_id() << " barcode = "   << (*pitr)->barcode() << " status = "  << (*pitr)->status());
          ATH_MSG_INFO(" a Parent mother "  << (*thisMother)->pdg_id()<< " barc = " << (*thisMother)->barcode());
          if ( (*thisMother)->pdg_id() == m_PDGGrandParent1 ) isGrandParent1OK = true;
        }
        ATH_MSG_INFO(" Grand Parent 1 is OK? " << isGrandParent1OK);
        if (!isGrandParent1OK) continue;
        ++nGoodParent;
        
      if (abs((*pitr)->pdg_id()) == m_PDGParent2 && (*pitr)->status() == 3) {
        HepMC::GenVertex::particle_iterator firstMother = (*pitr)->production_vertex()->particles_begin(HepMC::parents);
        HepMC::GenVertex::particle_iterator endMother = (*pitr)->production_vertex()->particles_end(HepMC::parents);
        HepMC::GenVertex::particle_iterator thisMother = firstMother;
        bool isGrandParent2OK = false;
        for (; thisMother != endMother; ++thisMother) {
          ATH_MSG_INFO(" Parent " << (*pitr)->pdg_id() << " barcode = "   << (*pitr)->barcode() << " status = "  << (*pitr)->status());
          ATH_MSG_INFO(" a Parent mother "  << (*thisMother)->pdg_id()<< " barc = " << (*thisMother)->barcode());
          if ( (*thisMother)->pdg_id() == m_PDGGrandParent2 ) isGrandParent2OK = true;
        }
        ATH_MSG_INFO(" Grand Parent 2 is OK? " << isGrandParent2OK);
        if (!isGrandParent2OK) continue;
        ++nGoodParent;

        HepMC::GenVertex::particle_iterator firstChild = (*pitr)->end_vertex()->particles_begin(HepMC::children);
        HepMC::GenVertex::particle_iterator endChild = (*pitr)->end_vertex()->particles_end(HepMC::children);
        HepMC::GenVertex::particle_iterator thisChild = firstChild;
        for (; thisChild != endChild; ++thisChild) {
          ATH_MSG_INFO(" child " << (*thisChild)->pdg_id());
          if (!okPDGChild1) {
            for (size_t i = 0; i < m_PDGChild1.size(); ++i)
              if (abs((*thisChild)->pdg_id()) == m_PDGChild1[i]) okPDGChild1 = true;
            if (okPDGChild1) break;
          }
        for (; thisChild !=endChild; ++thisChild) {
          ATH_MSG_INFO(" child " << (*thisChild)->pdg_id());
          if (!okPDGChild2) {
            for (size_t i = 0; i < m_PDGChild2.size(); ++i)
              if (abs((*thisChild)->pdg_id()) == m_PDGChild2[i]) okPDGChild2 = true;
            if (okPDGChild2) break;
          }
        }
      }
    }
  }

  ATH_MSG_INFO("Result " << nGoodParent << " " << okPDGChild1 << " " << okPDGChild2);

  if (nGoodParent == 2) ++m_nHNLtotau;
  if (nGoodParent == 2 && okPDGChild1 && okPDGChild2) {
    ++m_nGoodHNLtotau;
    return StatusCode::SUCCESS;
  }

  setFilterPassed(false);
  return StatusCode::SUCCESS;
}
